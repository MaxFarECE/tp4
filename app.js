$(document).ready(function(){

    function toggleNoDisp(){
        $(".description", this).toggleClass("no-display");
    }


    $(".pizza-type label").hover(toggleNoDisp,toggleNoDisp);

$('.nb-parts input').on('keyup', function(){

    $(".pizza-pict").remove();
    var pizza = $('<span class="pizza-pict"></span>');

    slices = +$(this).val();
    
    for(i=0; i < slices/6; i++){
        $(this).after(pizza.clone().addClass('pizza-6'));
    }

    if(slices%6 != 0){
            $('.pizza-pict').first().removeClass("pizza-6").addClass('pizza-'+slices%6);
    }
    
})

$(".next-step").click(function(){
    $(".next-step").addClass("no-display");
    $(".infos-client").removeClass("no-display");
})



function calculeTotal(){

    var typeValue = $("input[name='type']:checked").attr("data-price");
    var pateValue = $("input[name='pate']:checked").attr("data-price");
    var extraValue = $("input[name='extra']:checked").attr("data-price");
    var totalValue = parseInt(typeValue) + parseInt(pateValue) + parseInt(extraValue);
    $(".tile p").text(totalValue + " €"); 
 }

$("input[type='radio']").click(calculeTotal);

$(".infos-client .add").click(function(){
    $(this).before('<br><input type="text"/>');})

    
     

$(".infos-client .btn-success").click(
    function(){
        var name = $(".infos-client :first-child input").val();
        $(".main").addClass("no-display");
        $(".stick-right").addClass("no-display");
        $(".logo").append('<br><h2>Merci ' + name + '!<br>Votre commande sera livrée dans 15 minutes.</h2>').append();
        $('.row').find('input:text').val('');
    }
)

})
